package com.sda.mach.dariusz.javarze5.wstepdojava;

import com.sda.mach.dariusz.javarze5.wstepdojava.Day1.rozgrzewka.AgeUtils;

public class Application {

    public static void main(String[] args) {
        System.out.println(AgeUtils.printStatusForAge(18));
        System.out.println(AgeUtils.printStatusForAge(28));
        System.out.println(AgeUtils.printStatusForAge(16));
    }
}
