package com.sda.mach.dariusz.javarze5.wstepdojava.Day1;

import com.sda.mach.dariusz.javarze5.wstepdojava.Day1.oop.Employee;

import java.util.concurrent.TimeUnit;

public class StringExample {

    public static void main(String[] args) {
        System.out.println(1 + 2 + "3" + 4 + 5);
        System.out.println("" + 1 + 2 + 3 + 4);
        System.out.println("" + (1 + 2) + 3 + 4);

        Employee emp1 = new Employee("Dariusz Mach");
        Employee emp2 = new Employee("Katarzyna");
        System.out.println(emp1);
        System.out.println(emp2);

        // faster
        long start = System.nanoTime();

        fasterStringConcatinating();

        long end = System.nanoTime();
        long elapsedTime = end - start;
        long elapsedTimeInMilisecondsSecond = TimeUnit.MILLISECONDS.convert(elapsedTime, TimeUnit.NANOSECONDS);
        System.out.println(elapsedTimeInMilisecondsSecond + " milliseconds");


        // slower
        start = System.nanoTime();

        slowStringConcatinating();

        end = System.nanoTime();
        elapsedTime = end - start;
        // 1 second = 1_000_000_000 nano seconds
        double elapsedTimeInSecond = (double) elapsedTime / 1_000_000_000;
        System.out.println(elapsedTimeInSecond + " seconds");

    }


    public static void slowStringConcatinating() {
        String source = "";

        for (int i = 0; i < 100_000; i++) {
            source += i;
        }
    }

    public static void fasterStringConcatinating() {
        StringBuilder source = new StringBuilder();

        for (int i = 0; i < 10_000_000; i++) {
            source.append(i);
        }
    }
}
