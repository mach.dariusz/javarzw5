package com.sda.mach.dariusz.javarze5.wstepdojava.Day1.exercise;

public class Dog {
    private String name;
    private int age;
    private DogBreed breed;

    public Dog(String name, int age, DogBreed breed) {
        this.name = name;
        this.age = age;
        this.breed = breed;
    }

    // public Dog(String name, int age, DogBreed breed, boolean fat) {
    //     this(name, age, breed);
    //     System.out.println(fat ? "wow you are so fat" : "nice weight");
    // }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public DogBreed getBreed() {
        return breed;
    }

    public void bark() {
        switch (breed) {
            case CHIHUAHUA:
                System.out.println("wrrrrrrrrrr hau hau");
                break;
            default:
                System.out.println("hau hau");
                break;
        }
    }

    private String getDogInfo() {
        // StringBuffer sb = new StringBuffer();
        StringBuilder sb = new StringBuilder();

        sb.append("My dog: ");
        sb.append("name: ").append(getName());
        sb.append(", breed: ").append(getBreed())
                .append(", age: ").append(getAge());

        return sb.toString();
    }

    @Override
    public String toString() {

        return getDogInfo();
    }
}