package com.sda.mach.dariusz.javarze5.wstepdojava.Day1.exercise;

public enum DogBreed {
    BEAGLE, BULDOG_ANGIELSKI, PUDEL, LABRADOR, OWCZAREK_NIEMIECKI, CHIHUAHUA, MOPS, GOLDEN_RETREIVER
}
