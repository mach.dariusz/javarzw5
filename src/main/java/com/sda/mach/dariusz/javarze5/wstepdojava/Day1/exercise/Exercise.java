package com.sda.mach.dariusz.javarze5.wstepdojava.Day1.exercise;

/**
 * Stworz klase Dog - jeden konstruktor Dog(name, age, breed)
 * Dodaj pola: name, age, breed
 * Dodaj metody: getName(), getAge(), getBreed(), bark()
 * Nadpisz metode toString()
 * <p>
 * Stworz kilka obiektow klasy Dog i wypisz informacje o nich
 */
public class Exercise {

    public static void main(String[] args) {
        Dog labrador = new Dog("Goldi", 15, DogBreed.GOLDEN_RETREIVER);
        Dog chihuahua = new Dog("Chi", 10, DogBreed.CHIHUAHUA);
        System.out.println(labrador);
        System.out.println(chihuahua);
        labrador.bark();
        chihuahua.bark();

        // NullPointerException
        // Dog chihuahua2 = null;
        // chihuahua2.bark();

    }
}

