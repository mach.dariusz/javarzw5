package com.sda.mach.dariusz.javarze5.wstepdojava.Day1.oop;


public class Employee {

    public String name;

    public Employee(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        // return super.toString();
        return "Employee: " + this.name;
    }
}