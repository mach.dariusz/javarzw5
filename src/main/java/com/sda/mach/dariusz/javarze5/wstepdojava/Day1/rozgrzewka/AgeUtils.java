package com.sda.mach.dariusz.javarze5.wstepdojava.Day1.rozgrzewka;

public class AgeUtils {

    public static boolean checkAge(int age) {
        if (age >= 18) {
            return true;
        } else {
            return false;
        }
    }

    public static String printStatusForAge(int age) {
        if (checkAge(age)) {
            return "Jestes pelnoletni ...";
        } else {
            return "Jestes jeszcze zbyt mlody ...";
        }
    }

}