package com.sda.mach.dariusz.javarze5.wstepdojava.Day2.exceptions;

import java.util.Arrays;

public class App {

    public static void main(String[] args) {

        App.checkDocuments("dokument 1");
        App.checkDocuments("dokument 1", "", "dokument 3");
        App.checkDocuments("dokument 1","","","","","");
        App.checkDocuments();

        // try {
        //     App.buyAlcohol(18);
        //     App.buyAlcohol(17);
        // } catch (MyException e) {
        //     e.printStackTrace();
        //
        // }
    }


    public static void buyAlcohol(int age) throws MyException {
        if (age >= 18) {
            System.out.println("Sprzedalem alkohol ...");
        } else {
            throw new MyException("Osoba jest niepelnoletnia!");
        }
    }

    public static void checkDocuments(String... documents) {

        if (documents.length > 0) {
            // sprawdz je
            System.out.println("Sprawdzam dokumenty ...: " + Arrays.toString(documents));
        } else {
            throw new MyRuntimeException("Zapomniales o dokumentach!");
        }
    }
}
