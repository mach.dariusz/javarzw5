package com.sda.mach.dariusz.javarze5.wstepdojava.Day2.exceptions;

public class MyException extends Exception {

    public MyException(String message) {
        super(message);
    }
}
