package com.sda.mach.dariusz.javarze5.wstepdojava.Day2.exceptions;

public class MyRuntimeException extends RuntimeException {

    public MyRuntimeException(String message) {
        super(message);
    }
}
