package com.sda.mach.dariusz.javarze5.wstepdojava.Day2.exercise;

import com.sda.mach.dariusz.javarze5.wstepdojava.Day2.exercise.exceptions.MyCustomException;
import com.sda.mach.dariusz.javarze5.wstepdojava.Day2.exercise.exceptions.MyCustomRuntimeException;

public class App {

    public static void main(String[] args) {

        int results = StringUtils.checkNumberOfArguments();
        System.out.printf("Metoda przyjela %d argumentow.%n", results);
        System.out.println();
        System.out.println("-------------------------------");
        System.out.println();

        try {
            // operacje
            int resultOfSum = StringUtils.calculateSum(2, 2, 2);
            System.out.println("resultOfSum: " + resultOfSum);
            System.out.println();
            System.out.println("-------------------------------");
            System.out.println();

            int resultOfMultiplication = StringUtils.calculateMultiplication(2, 2, 2);
            System.out.println("resultOfMultiplication: " + resultOfMultiplication);
        } catch (MyCustomException e) {
            // przechwytujemy wyjatek
            System.out.println("Przechwycilem MyCustomException: " + e.getMessage());
        } catch (MyCustomRuntimeException e) {
            System.out.println("Przechwycilem MyCustomRuntimeException: " + e.getMessage());
        }

    }
}
