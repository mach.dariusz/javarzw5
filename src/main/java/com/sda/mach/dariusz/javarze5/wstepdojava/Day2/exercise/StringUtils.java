package com.sda.mach.dariusz.javarze5.wstepdojava.Day2.exercise;

import com.sda.mach.dariusz.javarze5.wstepdojava.Day2.exercise.exceptions.MyCustomException;
import com.sda.mach.dariusz.javarze5.wstepdojava.Day2.exercise.exceptions.MyCustomRuntimeException;

public class StringUtils {

    public static int checkNumberOfArguments(String... arguments) {
        return arguments.length;
    }

    public static int calculateSum(int... numbers) throws MyCustomException {

        if (numbers.length == 0) throw new MyCustomRuntimeException("Nie podano zadnych argumentow!");
        if (numbers.length == 1) throw new MyCustomException("Brakuje jeszcze przynajmniej jednego argumentu!");

        int result = 0;

        for (int i = 0; i < numbers.length; i++) {
            System.out.printf("Zamierzam dodac do %d liczbe %d%n", result, numbers[i]);
            result += numbers[i];
        }

        return result;
    }

    public static int calculateMultiplication(int... numbers) throws MyCustomException {

        if (numbers.length == 0) throw new MyCustomRuntimeException("Nie podano zadnych argumentow!");
        if (numbers.length == 1) throw new MyCustomException("Brakuje jeszcze przynajmniej jednego argumentu!");

        int result = 1;

        for (int i = 0; i < numbers.length; i++) {
            System.out.printf("Zamierzam pomnozyc %d przez liczbe %d%n", result, numbers[i]);
            result *= numbers[i];
        }

        return result;
    }
}
