package com.sda.mach.dariusz.javarze5.wstepdojava.Day2.exercise.exceptions;

public class MyCustomException extends Exception {

    public MyCustomException(String message) {
        super(message);
    }
}
