package com.sda.mach.dariusz.javarze5.wstepdojava.Day2.exercise.exceptions;

public class MyCustomRuntimeException extends RuntimeException {

    public MyCustomRuntimeException(String message) {
        super(message);
    }

}
