package com.sda.mach.dariusz.javarze5.wstepdojava.Day2.oop;

public class Animal {

    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public String getInfo() {
        return "Animal: " + this.name;
    }

    public String saySomething() {
        return "saySomething from Animal class";
    }
}
