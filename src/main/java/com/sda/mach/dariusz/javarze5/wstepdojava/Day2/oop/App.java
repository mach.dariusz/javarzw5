package com.sda.mach.dariusz.javarze5.wstepdojava.Day2.oop;

public class App {

    public static void main(String[] args) {
        Animal animal = new Animal("Zwierzak");
        System.out.println(animal.getInfo());
        System.out.println(animal.saySomething());

        System.out.println("---------------------");

        Cat cat = new Cat("Kociak");
        System.out.println(cat.getInfo());
        System.out.println(cat.saySomething());

        System.out.println("---------------------");

        Animal anotherCat = new Cat("Kocur");
        System.out.println(anotherCat.getInfo());
        System.out.println(anotherCat.saySomething());

        Animal[] arrayOfAnimals = new Animal[3];
        arrayOfAnimals[0] = anotherCat;
        arrayOfAnimals[1] = cat;
        arrayOfAnimals[2] = animal;

        ((Cat) anotherCat).sayMiau();

        // wil throw exception
        // Cat[] arrayOfCats = new Cat[3];
        // arrayOfCats[0] = (Cat) animal;


    }
}
