package com.sda.mach.dariusz.javarze5.wstepdojava.Day2.oop;

public class Cat extends Animal {

    public Cat(String name) {
        super(name);
    }

    @Override
    public String getInfo() {
        return "Cat: \n\t" + super.getInfo();
    }

    public void sayMiau() {
        System.out.println("miau miau");
    }
}
