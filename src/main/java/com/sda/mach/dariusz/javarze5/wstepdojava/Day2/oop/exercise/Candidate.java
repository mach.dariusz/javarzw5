package com.sda.mach.dariusz.javarze5.wstepdojava.Day2.oop.exercise;

public class Candidate {

    String name;

    String getInfo() {
        String name = "Maurycy";
        return "Candidate: " + name;
    }
}

class Employee extends Candidate {

    String name = "Wlodzislaw";

    @Override
    String getInfo() {

        return "Employee: " + super.name;
    }
}

class Manager extends Employee {

    @Override
    String getInfo() {
        return "Manager: " + super.name;
    }
}









class App {
    public static void main(String[] args) {
        Candidate candidate = new Candidate();
        candidate.name = "Dariusz Mach";

        Candidate candidate1 = new Employee();
        candidate1.name = candidate.name;

        Candidate candidate2 = new Manager();
        candidate2.name = candidate.name;

        System.out.println(candidate.getInfo());
        System.out.println(candidate1.getInfo());
        System.out.println(candidate2.getInfo());

        candidate.name = "Piotr";

        System.out.println(candidate.getInfo());
        System.out.println(candidate1.getInfo());
        System.out.println(candidate2.getInfo());

    }
}
