package com.sda.mach.dariusz.javarze5.wstepdojava.Day2.rozgrzewka;

public class App {

    public static void main(String[] args) {

        Employee emp1 = new Employee("Dariusz", "Mach");
        Employee emp2 = new Employee("Miłosz", "Mach", 100.00);

        emp1.setSalary(500.00);

        System.out.println(emp1);
        System.out.println(emp2);
    }
}
