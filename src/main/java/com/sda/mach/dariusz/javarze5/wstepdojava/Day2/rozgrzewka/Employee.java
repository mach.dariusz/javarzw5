package com.sda.mach.dariusz.javarze5.wstepdojava.Day2.rozgrzewka;

/**
 * Napisz klase Employee
 *
 * Pola: firstName, lastName, salary
 *
 * Employee(firstName, lastName)
 *
 * main -> 2 emps
 *
 */
public class Employee {

    private String firstName;
    private String lastName;
    private double salary;

    public Employee(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Employee(String firstName, String lastName, double salary) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.salary = salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }

    @Override
    public String toString() {
        return "Employee: [ firstName: " + this.firstName + " | lastName: " + this.lastName + " ]";
    }
}


