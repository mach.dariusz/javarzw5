package com.sda.mach.dariusz.javarze5.wstepdojava.Day2.staticBlocks;

public class App {

    public static void main(String[] args) {

        Employee e = new Employee();
        e.getInfo();
    }

}


class Employee {

    static {
        System.out.println("static block");
    }

    Employee() {
        System.out.println("constructor");
    }

    void getInfo() {
        System.out.println("getInfo()");
    }

}