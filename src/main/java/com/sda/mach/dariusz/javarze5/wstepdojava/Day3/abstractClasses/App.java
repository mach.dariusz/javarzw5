package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.abstractClasses;

public class App {

    public static void main(String[] args) {
        Vehicle motorbike = new MotorBike();
        System.out.println(motorbike.getEfficiency());

        Vehicle car = new Car();
        System.out.println(car.getEfficiency());
    }
}
