package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.abstractClasses;

public abstract class Vehicle {

    public abstract double getDistance();
    public abstract double getFuelUsage();

    public double getEfficiency() {
        return getFuelUsage() / getDistance();
    }
}

class MotorBike extends Vehicle {

    @Override
    public double getDistance() {
        return 250;
    }

    @Override
    public double getFuelUsage() {
        return 2;
    }
}

class Car extends Vehicle {

    @Override
    public double getDistance() {
        return 800;
    }

    @Override
    public double getFuelUsage() {
        return 9;
    }
}


