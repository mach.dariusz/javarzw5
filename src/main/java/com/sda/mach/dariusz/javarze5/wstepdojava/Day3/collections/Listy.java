package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.collections;

import java.util.*;

public class Listy {

    public static void main(String[] args) {
        arrayList();
        linkedList();
    }

    static void arrayList() {
        List<String> list = new ArrayList<>();
        // ArrayList<String> list = new ArrayList<>();

        list.add("Kasia");
        list.add("Darek");
        list.add("Marek");

        for (String item: list) {
            System.out.println(item);
        }
    }

    static void linkedList() {
        List<String> list = new LinkedList<>();

        list.add("Kasia");
        list.add("Darek");
        list.add("Marek");

        for (String item: list) {
            System.out.println(item);
        }
    }
}
