package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.collections;

import java.util.*;

public class Mapy {

    public static void main(String[] args) {
        stringExample();
        integerExample();

        humanExample();
        humanExample2();

        putToMap();

        iterationOverMap();
        iterationOverMap2();
        iterationOverMap3();

        zadanie();

    }

    static void stringExample() {
        Map<Integer, String> map = new HashMap<>();

        map.put(1, "Ala");
        map.put(2, "Darek");

        System.out.println(map.get(1));
        System.out.println(map.get(2));
    }

    static void integerExample() {
        Map<Integer, Integer> map = new HashMap<>();

        map.put(1, 1 * 1);
        map.put(2, 2 * 2);
        map.put(3, 3 * 3);

        System.out.println(map.get(3));
    }

    static void humanExample() {
        Map<String, Human> map = new HashMap<>();

        map.put("DM", new Human("Dariusz Mach"));
        // nadpisze stara wartosc dla klucza DM
        map.put("DM", new Human("Dariusz Mach"));

        System.out.println(map.get("DM"));

    }

    static void humanExample2() {
        Map<Human, String> map = new HashMap<>();

        map.put(new Human("Darek Mach"), "DM");
        if (!map.containsKey(new Human("Dariusz Mach"))) {
            map.put(new Human("Dariusz Mach"), "DDM");
        }

        System.out.println(map.get(new Human("Dariusz Mach")));
    }

    static void iterationOverMap() {
        Map<Integer, String> map = new HashMap<>();

        map.put(1, "A");
        map.put(2, "B");
        map.put(3, "C");
        map.put(4, "D");

        System.out.println(map.keySet());

        for (Integer key : map.keySet()) {
            System.out.println("klucz: " + key + " - wartosc: " + map.get(key));
        }
    }

    static void iterationOverMap2() {
        Map<Integer, String> map = new HashMap<>();

        map.put(1, "A");
        map.put(2, "B");
        map.put(3, "C");
        map.put(4, "D");

        System.out.println(map.values());

        for (String value : map.values()) {
            System.out.println(value);
        }
    }

    static void iterationOverMap3() {
        Map<Integer, String> map = new HashMap<>();

        map.put(1, "A");
        map.put(2, "B");
        map.put(3, "C");
        map.put(4, "D");

        System.out.println(map.entrySet());

        for (Map.Entry<Integer, String> entry : map.entrySet()) {
            System.out.println("key: " + entry.getKey() + " value: " + entry.getValue());
        }

    }

    static void putToMap() {
        Map<Integer, String> map = new HashMap<>();

        for (int i = 0; i < 10; i++) {
            map.put(i, "Wartosc " + i);
        }

        System.out.println(map.keySet());
        System.out.println(map.values());
    }

    static void zadanie() {
        // sprawdzic ilosc wystapien String'ow w liscie
        List<String> lista = Arrays.asList("A", "A", "B", "C", "C", "E", "Z", "Z", "Z");

        Map<String, Integer> map = new HashMap<>();
        for (String item : lista) {
            if (map.containsKey(item)) {
                map.put(item, map.get(item) + 1);
            } else {
                map.put(item, 1);
            }
        }

        System.out.println(map.entrySet());
    }

}

class Human {
    private String name;

    Human(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return Objects.equals(name, human.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return this.name;
    }
}