package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.collections;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class Sety {

    public static void main(String[] args) {

        stringExample();
        employeeExample();
    }

    static void stringExample() {
        Set<String> set = new HashSet<>();

        set.add("Darek");
        set.add("Darek");
        set.add("Marek");
        set.add("Arek");
        set.add("Darek");
        set.add("Darek");
        set.add("Darek");
        set.add("Darek");
        set.add("Darek");
        set.add("Darek");
        set.add("Darek");
        set.add("Darek");
        set.add("Darek");
        set.add("Darek");

        System.out.println(set);
    }


    static void employeeExample() {

        Set<Employee> set = new HashSet<>();
        set.add(new Employee("Darek", "Mach"));
        set.add(new Employee("Darek", "Mach"));
        set.add(new Employee("Darek", "Mach"));

        System.out.println(set);
    }
}


class Employee {
    private String firstName;
    private String lastName;

    Employee(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Employee employee = (Employee) o;
        return Objects.equals(firstName, employee.firstName) &&
                Objects.equals(lastName, employee.lastName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName);
    }

    @Override
    public String toString() {
        return "Employee{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                '}';
    }
}