package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.generics;

public class App {

    public static void main(String[] args) {

        objectExample();
        integerExample();
        personExample();

    }

    static void objectExample() {
        IStack stos = new Stack();

        stos.add(1);
        stos.add(2);
        stos.add("DAREK");
        stos.add('A');


        for (Object o: stos.getAllItems()) {
            if (o instanceof String) {
                System.out.println("String: " + o);
            } else if (o instanceof Character) {
                System.out.println("character: " + o);
            } else if (o instanceof Integer) {
                System.out.println("Liczba: " + o);
            }
        }
    }

    static void integerExample() {
        IStack<Integer> stos = new Stack<>();

        stos.add(1);
        stos.add(2);
        stos.add(3);

        System.out.println(stos.getAllItems());
    }


    static void personExample() {
        IStack<Person> stos = new Stack<>();

        Person p1 = new Person();
        p1.name = "Dariusz Mach";

        stos.add(p1);

        System.out.println(stos.getAllItems());

    }
}

class Person {
    String name;

    @Override
    public String toString() {
        return this.name;
    }
}


