package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.generics;

import java.util.ArrayList;
import java.util.List;

public interface IStack<T> {

    void add(T item);

    List<T> getAllItems();
}

class Stack<T> implements IStack<T> {

    private final List<T> list = new ArrayList<>();

    @Override
    public void add(T item) {
        list.add(item);
    }

    @Override
    public List<T> getAllItems() {
        return this.list;
    }
}
