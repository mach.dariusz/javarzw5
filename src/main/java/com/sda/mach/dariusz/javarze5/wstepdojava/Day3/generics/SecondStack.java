package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.generics;

import java.util.List;

public class SecondStack<T> implements CustomStack<T> {
    private List<T> list;

    @Override
    public void add(T item) {
        this.list.add(item);
    }


    public static void main(String[] args) {
        CustomStack<String> stackOfStrings = new SecondStack<>();
        CustomStack<Integer> stackOfIntegers = new SecondStack<>();
    }
}

interface CustomStack<T> {
    void add(T item);
}


