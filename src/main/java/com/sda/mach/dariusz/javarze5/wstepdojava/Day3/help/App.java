package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.help;

public class App {

    public static void main(String[] args) {
        Person person = new Person(new Heart(), new Hand(), new Hand());

        City city = new City();
        city.add(new Car());
        city.add(new Car());
        city.add(new Car());
        city.add(new Tree());
        city.add(new Tree());
        city.add(new Tree());
        city.add(new Tree());
        city.add(new Tree());
        city.add(new Tree());
        city.add(new Tree());

        System.out.println(city.getInfo());
    }
}
