package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.help;

import java.util.ArrayList;
import java.util.List;

public class City {
    private List<Tree> trees;
    private List<Car> cars;

    public City() {
        trees = new ArrayList<>();
        cars = new ArrayList<>();
    }

    void add(Tree tree) {
        System.out.println("Added new tree ...");
        trees.add(tree);
    }

    void add(Car car) {
        System.out.println("Added new car ...");
        cars.add(car);
    }

    String getInfo() {
        StringBuilder sb = new StringBuilder();
        sb.append("Nasze miasto posiada:\n\t");
        sb.append("- ").append(cars.size());

        if (cars.size() == 1) {
            sb.append(" samochod\n\t");
        } else if (cars.size() == 2 && cars.size() == 3 && cars.size() == 4) {
            sb.append(" samochody\n\t");
        } else {
            sb.append(" samochodów\n\t");
        }

        sb.append("- ").append(trees.size());

        if (trees.size() == 1) {
            sb.append(" drzewo\n\t");
        } else if (trees.size() == 2 && trees.size() == 3 && trees.size() == 4) {
            sb.append(" drzewa\n\t");
        } else {
            sb.append(" drzew\n\t");
        }

        return sb.toString();
    }
}
