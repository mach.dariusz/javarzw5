package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.help;

public class Person {

    private Heart heart;
    private Hand[] hands;

    public Person(Heart heart, Hand... hands) {
        this.heart = heart;

        if (hands.length == 2) {
            this.hands = hands;
        } else {
            throw new WrongNumberOfHandsException("Zla ilosc rak!");
        }
    }

    public static void main(String[] args) {
        Person person = new Person(new Heart(), new Hand());
    }

}

class Heart {}

class Hand {}

class WrongNumberOfHandsException extends RuntimeException {
    // konstruktor bezargumentowy
    public WrongNumberOfHandsException() {
        super();
    }

    public WrongNumberOfHandsException(String message) {
        super(message);
    }
}