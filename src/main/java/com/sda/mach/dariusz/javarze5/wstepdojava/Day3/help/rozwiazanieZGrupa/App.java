package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.help.rozwiazanieZGrupa;


public class App {

    public static void main(String[] args) {
        City city = new City();

        Tree dab = new Tree();
        Tree sosna = new Tree();

        city.add(dab);
        city.add(sosna);

        Car vw = new Car("VW");
        Car seat = new Car("SEAT");
        
        city.add(vw);
        city.add(seat);

        System.out.println(city.getInfo());
    }
}
