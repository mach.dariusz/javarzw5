package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.help.rozwiazanieZGrupa;

import java.util.ArrayList;
import java.util.List;

public class City {

    private List<Tree> trees;
    private List<Car> cars;

    public City() {
        this.trees = new ArrayList<>();
        this.cars = new ArrayList<>();
    }

    public void add(Tree tree) {
        this.trees.add(tree);
    }

    public void add(Car car) {
        this.cars.add(car);
    }

    public String getInfo() {
        // StringBuilder sb = new StringBuilder();
        // sb.append("City have: \n");
        // sb.append("\n\tTrees: ").append(this.trees);
        // sb.append("\n\tCars: ").append(this.cars);
        //
        // return sb.toString();

        return "trees: " + this.trees
                + "\ncars: " + this.cars;
    }
}


class Tree {
    @Override
    public String toString() {
        return "drzewo";
    }
}

class Car {
    private String name;

    Car(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}