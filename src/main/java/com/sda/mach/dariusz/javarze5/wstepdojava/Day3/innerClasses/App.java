package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.innerClasses;

public class App {

    public static void main(String[] args) {
        new Human.Heart();
        new Human("Darek").new Hand();


        LazySingleton singleton = LazySingleton.getInstance();
    }
}


class Human {

    private final String name;

    public Human(String name) {
        this.name = name;
    }


    static class Heart {
        Heart() {
            System.out.println("static Heart");
        }
    }

    class Hand {
        Hand() {
            System.out.println("Hand " + name);
        }
    }
}



