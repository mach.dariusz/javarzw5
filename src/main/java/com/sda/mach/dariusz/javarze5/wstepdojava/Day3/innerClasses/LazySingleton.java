package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.innerClasses;

public class LazySingleton {
    private LazySingleton() {}

    private static class LazyHolder {
        static final LazySingleton INSTANCE = new LazySingleton();
    }

    public static LazySingleton getInstance() {
        return LazyHolder.INSTANCE;
    }
}