package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.interfaces;

public class App {

    public static void main(String[] args) {
        MyInterface interface1 = new Mnozenie();
        System.out.println(interface1.calculate(10));
        interface1.metodaDefaultowa();

        MyInterface myInterface2 = new DzieleniePrzezDwa();
        System.out.println(myInterface2.calculate(10));
        myInterface2.metodaDefaultowa();


        MyInterface.metodaStatyczna();
    }
}
