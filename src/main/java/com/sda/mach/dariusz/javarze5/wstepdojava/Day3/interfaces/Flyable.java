package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.interfaces;

public interface Flyable {
    void fly();
    void foo();
}

class Bird implements Flyable {

    @Override
    public void fly() {
        System.out.println("I am flying like a bird");
    }

    @Override
    public void foo() {

    }
}

class Plane implements Flyable {

    @Override
    public void fly() {
        System.out.println("I am flying like a plane");
    }

    @Override
    public void foo() {

    }
}