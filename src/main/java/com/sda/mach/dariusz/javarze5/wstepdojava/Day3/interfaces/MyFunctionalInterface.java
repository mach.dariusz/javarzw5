package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.interfaces;

@FunctionalInterface
public interface MyFunctionalInterface {

    void foo();

    static void bar() {
        System.out.println();
    };
    static void bar2() {};
    static void bar3() {};
    default void defaultMethod() {
        System.out.println();
    };
    default void defaultMethod2() {};
    default void defaultMethod3() {};
}
