package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.interfaces;



public interface MyInterface {

    double calculate(double x);

    static void metodaStatyczna() {
        System.out.println("wywolano z metody statycznej");
    }

    default void metodaDefaultowa() {
        System.out.println("wywolano z metody defaultowej");
    }
}

class Mnozenie implements MyInterface {

    @Override
    public double calculate(double x) {
        return x * x;
    }

    @Override
    public void metodaDefaultowa() {
        System.out.println("wywolano z metody defaultowej napisanej w klasie Mnozenie");
    }
}

class DzieleniePrzezDwa implements MyInterface {

    @Override
    public double calculate(double x) {
        return x / 2;
    }
}
