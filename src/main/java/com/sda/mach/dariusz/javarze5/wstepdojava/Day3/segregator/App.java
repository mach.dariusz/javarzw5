package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.segregator;

import com.sda.mach.dariusz.javarze5.wstepdojava.Day3.segregator.document.Dokument;
import com.sda.mach.dariusz.javarze5.wstepdojava.Day3.segregator.document.DokumentInny;
import com.sda.mach.dariusz.javarze5.wstepdojava.Day3.segregator.document.DokumentUS;
import com.sda.mach.dariusz.javarze5.wstepdojava.Day3.segregator.document.Faktura;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class App {

    public static void main(String[] args) {
        Szafa szafa = new Szafa();

        Segregator segregatorNaFaktury = new Segregator(Typ.FAKTURA);
        Segregator segregatorNaRozneDokumenty = new Segregator(Typ.MIESZANY);

        Dokument doc1 = new Faktura("FV 1/1/2019", new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime());
        Dokument doc2 = new Faktura("FV 2/1/2019", new GregorianCalendar(2019, Calendar.JANUARY, 2).getTime());
        Dokument doc3 = new Faktura("FV 3/1/2019", new GregorianCalendar(2019, Calendar.JANUARY, 3).getTime());
        Dokument doc4 = new DokumentUS("US 1/1/2019", new GregorianCalendar(2019, Calendar.JANUARY, 1).getTime());
        Dokument doc5 = new DokumentInny("INNY 1/2/2019", new GregorianCalendar(2019, Calendar.FEBRUARY, 20).getTime());

        segregatorNaFaktury.add(new Koszulka(doc1));
        segregatorNaFaktury.add(new Koszulka(doc2));
        segregatorNaFaktury.add(new Koszulka(doc3));

        segregatorNaRozneDokumenty.add(new Koszulka(doc4));
        segregatorNaRozneDokumenty.add(new Koszulka(doc5));

        szafa.add(segregatorNaFaktury);
        szafa.add(segregatorNaRozneDokumenty);

        System.out.println(szafa.getInfo());
        System.out.println("Segregator na faktury posiada dokumentów: " + segregatorNaFaktury.getAll().size());
        System.out.println("Segregator na różne dokumenty posiada dokumentów: " + segregatorNaRozneDokumenty.getAll().size());

        System.out.println("Dokument doc1: " + segregatorNaFaktury.get(doc1).get());
        System.out.println("Dokument doc5: " + segregatorNaRozneDokumenty.get(doc5).get());

    }
}
