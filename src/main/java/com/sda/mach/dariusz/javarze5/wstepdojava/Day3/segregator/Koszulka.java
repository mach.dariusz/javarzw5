package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.segregator;

import com.sda.mach.dariusz.javarze5.wstepdojava.Day3.segregator.document.Dokument;

import java.util.Objects;

public class Koszulka {

    private Dokument document;

    public Koszulka() {}

    public Koszulka(Dokument document) {
        this.document = document;
    }

    public void put(Dokument item) {
        this.document = item;
    }

    public Dokument get() {
        return this.document;
    }

    public void remove() {
        this.document = null;
    }

    @Override
    public String toString() {
        return "\n\t\tKoszulka{" +
                "document=" + document +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Koszulka koszulka = (Koszulka) o;
        return Objects.equals(document, koszulka.document);
    }

    @Override
    public int hashCode() {
        return Objects.hash(document);
    }
}
