package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.segregator;

import com.sda.mach.dariusz.javarze5.wstepdojava.Day3.segregator.document.Dokument;

import java.util.ArrayList;
import java.util.List;

public class Segregator {

    private Typ type;
    private List<Koszulka> items;

    public Segregator(Typ type) {
        this.type = type;
        this.items = new ArrayList<>();
    }

    public void add(Koszulka item) {
        this.items.add(item);
    }

    public Koszulka get(Dokument document) {
        return this.items.stream().filter(koszulka -> koszulka.get().equals(document)).findAny().orElse(null);
    }

    public boolean remove(Dokument item) {
        return this.items.remove(new Koszulka(item));
    }

    public List<Koszulka> getAll() {
        return this.items;
    }

    @Override
    public String toString() {
        return "Segregator{" +
                "type=" + type +
                ", \n\titems=" + items +
                "\n\t}";
    }

    public Typ getType() {
        return type;
    }
}
