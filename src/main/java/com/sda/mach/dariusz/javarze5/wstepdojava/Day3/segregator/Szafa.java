package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.segregator;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Szafa {

    private List<Segregator> items;

    public Szafa() {
        this.items = new ArrayList<>();
    }

    public void add(Segregator item) {
        this.items.add(item);
    }

    public List<Segregator> get(Typ type) {
        return this.items.stream().filter(segregator -> segregator.getType().equals(type)).collect(Collectors.toList());
    }

    public boolean remove(Segregator item) {
        return this.items.remove(item);
    }

    public String getInfo() {
        return "Szafa posiada segregatorów: " + this.items.size();
    };
}
