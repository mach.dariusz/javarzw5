package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.segregator;

public enum Typ {
    FAKTURA,
    PARAGON,
    POCZTA_PRZYCHODZACA,
    POCZTA_WYCHODZACA,
    POTWIERDZENIE,
    ZUS,
    US,
    MIESZANY
}
