package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.segregator.document;

import com.sda.mach.dariusz.javarze5.wstepdojava.Day3.segregator.Typ;

import java.util.Date;
import java.util.Objects;

public abstract class Dokument {

    private String id;
    private Date date;
    protected Typ type;

    public Dokument(String id, Date date) {
        this.id = id;
        this.date = date;
        this.setType();
    }

    public abstract void setType();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dokument dokument = (Dokument) o;
        return Objects.equals(id, dokument.id) &&
                type == dokument.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type);
    }

    @Override
    public String toString() {
        return "Dokument{" +
                "id='" + id + '\'' +
                ", date=" + date +
                ", type=" + type +
                '}';
    }
}
