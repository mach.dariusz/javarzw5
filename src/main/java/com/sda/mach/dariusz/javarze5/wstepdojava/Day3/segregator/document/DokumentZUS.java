package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.segregator.document;

import com.sda.mach.dariusz.javarze5.wstepdojava.Day3.segregator.Typ;

import java.util.Date;

public class DokumentZUS extends Dokument {

    public DokumentZUS(String id, Date date) {
        super(id, date);
    }

    @Override
    public void setType() {
        this.type = Typ.ZUS;
    }
}
