package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.segregator.document;

import com.sda.mach.dariusz.javarze5.wstepdojava.Day3.segregator.Typ;

import java.util.Date;

public class Paragon extends Dokument {

    public Paragon(String id, Date date) {
        super(id, date);
    }

    @Override
    public void setType() {
        this.type = Typ.PARAGON;
    }
}
