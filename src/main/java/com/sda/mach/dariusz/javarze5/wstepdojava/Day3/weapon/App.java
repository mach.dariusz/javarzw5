package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.weapon;

import java.util.Arrays;
import java.util.List;

public class App {

    /**
     * Composition - When an object is composed of other objects
     *               and the owning object is destroyed,
     *               the objects that are part of the composition go away, too.
     */

    public static void main(String[] args) {
        // This unit is composed of instance of Weapon
        // This Gun object does not exist outside of the context
        Unit soldier = new Unit(UnitType.SOLDIER, new Gun());
        Unit knight = new Unit(UnitType.KNIGHT, new Sword());
        Unit peasant = new Unit(UnitType.PEASANT, new Club());

        List<Unit> units = Arrays.asList(soldier, knight, peasant);
        units.forEach(Unit::attack);
    }
}
