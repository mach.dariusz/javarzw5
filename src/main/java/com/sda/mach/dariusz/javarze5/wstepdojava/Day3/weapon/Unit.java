package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.weapon;

public class Unit {
    private UnitType type;
    private Weapon weapon;

    public Unit(UnitType unit, Weapon weapon) {
        this.type = type;
        this.weapon = weapon;
    }

    void attack() {
        this.weapon.attack();
    }
}
