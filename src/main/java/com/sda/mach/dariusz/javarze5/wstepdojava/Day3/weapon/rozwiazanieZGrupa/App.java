package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.weapon.rozwiazanieZGrupa;

import java.util.Arrays;
import java.util.List;

public class App {

    public static void main(String[] args) {
        CUnit soldier = new CUnit(EUnitType.SOLDIER, new CGun());
        CUnit knight = new CUnit(EUnitType.KNIGHT, new CSword());
        CUnit peasant = new CUnit(EUnitType.PEASANT, new CClub());

        List<CUnit> list = Arrays.asList(soldier, knight, peasant);

        for (CUnit unit: list) {
            unit.attack();
        }

        // list.forEach(unit -> unit.attack());
    }
}
