package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.weapon.rozwiazanieZGrupa;

public class CUnit {
    private EUnitType type;
    private IWeapon weapon;

    public CUnit(EUnitType type, IWeapon weapon) {
        this.type = type;
        this.weapon = weapon;
    }

    public void attack() {
        this.weapon.attack();
    }
}
