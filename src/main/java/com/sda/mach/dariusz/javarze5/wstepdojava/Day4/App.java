package com.sda.mach.dariusz.javarze5.wstepdojava.Day4;

import com.sda.mach.dariusz.javarze5.wstepdojava.Day4.zadanie.*;

public class App {

    public static void main(String[] args) {

        Student ciezkoPracujacy = new Student("Kasia", "Kowalska");
        Student len = new Student("Darek", "Mach");

        Activity pracaNaZajaciach = new NaZajeciach();
        Activity pracaWDomu = new WDomu();
        Activity wolne = new Wolne();

        ciezkoPracujacy.addActivity(pracaNaZajaciach);
        ciezkoPracujacy.addActivity(pracaWDomu);

        len.addActivity(wolne);

        ciezkoPracujacy.executeActivities();
        len.executeActivities();
    }

}