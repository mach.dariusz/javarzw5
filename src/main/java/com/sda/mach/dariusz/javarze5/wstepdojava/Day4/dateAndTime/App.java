package com.sda.mach.dariusz.javarze5.wstepdojava.Day4.dateAndTime;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

public class App {

    public static void main(String[] args) throws InterruptedException {
        App app = new App();

        app.localDate();
        System.out.println();

        app.localTime();
        System.out.println();

        app.instantAndDuration();
        System.out.println();

        app.stringFormatter();
        System.out.println();

        app.period();
        System.out.println();
    }

    void localDate() {
        LocalDate date = LocalDate.now();
        System.out.println("LocalDate.now(): " + date);

        LocalDate date_2 = LocalDate.of(2019, 02, 22);
        System.out.println("LocalDate.of(2019,02, 22): " + date_2);
    }

    void localTime() {
        LocalTime time = LocalTime.now();
        System.out.println("LocalTime.now(): " + time);

        LocalTime time2 = LocalTime.of(21, 45, 15);
        System.out.println("LocalTime.of(21,45, 15): " + time2);
    }

    void instantAndDuration() throws InterruptedException {
        Instant start = Instant.now();
        System.out.println("Started instantAndDuration() at: " + LocalDateTime.ofInstant(start, ZoneOffset.UTC).toLocalTime());

        TimeUnit.SECONDS.sleep(2);

        Instant end = Instant.now();
        System.out.println("Finished instantAndDuration() at: " + LocalDateTime.ofInstant(end, ZoneOffset.UTC).toLocalTime());

        long duration = Duration.between(start, end).toMillis();
        System.out.println("Duration: " + duration + " milliseconds");
    }

    void stringFormatter() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMyyyy");
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("dd MMM yyyy");

        String dateAsString = "12071986";

        LocalDate date = LocalDate.parse(dateAsString, formatter);
        System.out.println("LocalDate.parse(12071986, ddMMyyyy): " + date);

        System.out.println("LocalDate.parse(12071986, ddMMyyyy).format(dd MMM yyyy): " + date.format(formatter2));
    }

    void period() {
        LocalDate now = LocalDate.now();
        LocalDate birthday = LocalDate.of(1986, 07, 12);

        Period period = Period.between(birthday, now);

        System.out.println(period.getYears() + " years");
        System.out.println(period.getMonths() + " months");
        System.out.println(period.getDays() + " days");

        long daysElapsed = ChronoUnit.DAYS.between(birthday, now);
        System.out.println(daysElapsed + " days elapsed");
    }
}
