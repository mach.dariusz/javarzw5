package com.sda.mach.dariusz.javarze5.wstepdojava.Day4.dateAndTime;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

public class App2 {

    public static void main(String[] args) throws InterruptedException {
        localDate();
        localTime();

        instantAndDuration();

        period();

        stringFormatter();
    }

    static void localDate() {
        LocalDate date = LocalDate.now();
        System.out.println("date: " + date);

        LocalDate myDate = LocalDate.of(1986, 7, 12);
        LocalDate myDate2 = LocalDate.of(1986, Month.JULY, 12);

        System.out.println("myDate: " + myDate);
        System.out.println("myDate2: " + myDate2);
    }

    static void localTime() {

        LocalTime time = LocalTime.now();
        System.out.println("time: " + time);

        LocalTime myTime = LocalTime.of(13,15,34);
        System.out.println("myTime: " + myTime);
    }

    static void instantAndDuration() throws InterruptedException {
        Instant start = Instant.now();
        System.out.println("Started instantAndDuration() at: " + LocalDateTime.ofInstant(start, ZoneOffset.UTC).toLocalTime());

        TimeUnit.SECONDS.sleep(2);

        Instant end = Instant.now();
        System.out.println("Finished instantAndDuration() at: " + LocalDateTime.ofInstant(end, ZoneOffset.UTC).toLocalTime());

        long duration = Duration.between(start, end).toMillis();
        System.out.println("Duration: " + duration + " milliseconds");
    }

    static void period() {
        LocalDate now = LocalDate.now();
        LocalDate birthday = LocalDate.of(1986, Month.JULY, 12);

        Period periodBetweenNowAndBirthday = Period.between(birthday, now);

        System.out.println(periodBetweenNowAndBirthday.getYears() + " years");
        System.out.println(periodBetweenNowAndBirthday.getMonths() + " months");
        System.out.println(periodBetweenNowAndBirthday.getDays() + " days");

        long daysElapsed = ChronoUnit.DAYS.between(birthday, now);
        System.out.println(daysElapsed + " days elapsed");
    }

    static void stringFormatter() {
        String dateAsString = "09071986";

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("ddMMyyyy");
        DateTimeFormatter formatter2 = DateTimeFormatter.ofPattern("dd MMM yyyy");

        LocalDate parsedDate = LocalDate.parse(dateAsString, formatter);
        System.out.println("LocalDate.parse(12071986, ddMMyyyy): " + parsedDate);

        System.out.println("LocalDate.parse(12071986, ddMMyyyy).format(dd MMM yyyy): "
                + parsedDate.format(formatter2));
    }
}
