package com.sda.mach.dariusz.javarze5.wstepdojava.Day4.nio;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class App {

    public static void main(String[] args) throws IOException {


        final RandomAccessFile sampleFile = new RandomAccessFile("sample.txt", "rw");
        final FileChannel channel = sampleFile.getChannel();
        final ByteBuffer buffer = ByteBuffer.allocate(1024);

        writeToFile(channel, buffer);
        readFromFile(channel, buffer);

        sampleFile.close();
    }

    // reading file sample.txt
    static void readFromFile(FileChannel channel, ByteBuffer buffer) throws IOException {
        int numberOfBytes = channel.read(buffer);

        while (numberOfBytes != -1) {
            System.out.println("Read: " + numberOfBytes);
            buffer.flip();
            while (buffer.hasRemaining()) {
                System.out.print((char) buffer.get());
            }

            buffer.clear();
            numberOfBytes = channel.read(buffer);
        }
    }

    // writing file sample.txt
    static void writeToFile(FileChannel channel, ByteBuffer buffer) throws IOException {
        String text = "\nAdded to file";
        buffer.put(text.getBytes());
        buffer.flip();
        channel.write(buffer, channel.size());
        buffer.clear();
    }
}
