package com.sda.mach.dariusz.javarze5.wstepdojava.Day4.regexp;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class App {


    public static void main(String[] args) {
        first();
        second();
        third();
        fourth();
    }


    static void first() {
        Pattern p = Pattern.compile("ko*tek");
        Matcher m = p.matcher("koooooooooooooootek");
        System.out.println(m.matches());
    }

    static void second() {

        String wyraz = "one,two, three four , five";
        Pattern p = Pattern.compile("[,\\s]+");
        String[] result = p.split(wyraz);
        for (String item: result) {
            System.out.println("|" + item + "|");
        }

    }

    static void third() {
        String wyraz = "Raz1dwa124trzy";
        String[] d1 = wyraz.split("d");
        String[] d2 = wyraz.split("\\d");

        System.out.println(Arrays.toString(d1));
        System.out.println(Arrays.toString(d2));

    }

    static void fourth() {
        String wyraz = "wyraz,wyraz2,wyraz3,wyraz4,...,n";
        String[] strArray = wyraz.split(",");
        System.out.println(Arrays.toString(strArray));
    }
    
}
