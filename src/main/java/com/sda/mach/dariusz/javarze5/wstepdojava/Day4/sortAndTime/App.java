package com.sda.mach.dariusz.javarze5.wstepdojava.Day4.sortAndTime;

import java.security.SecureRandom;
import java.text.NumberFormat;
import java.time.Duration;
import java.time.Instant;
import java.util.Arrays;

public class App {

    public static void main(String[] args) {
        App app = new App();

        SecureRandom random = new SecureRandom();
        int[] array_1 = random.ints(15_000_000).toArray();
        int[] array_2 = new int[array_1.length];
        System.arraycopy(array_1, 0, array_2, 0, array_1.length);

        long normalSort = app.sort(array_1);
        long parallelSort = app.parallelSort(array_2);

        // why casting to double?
        String percentage = NumberFormat.getPercentInstance().format((double) normalSort / parallelSort);
        System.out.printf("Sort took %s more time than parallel sort%n", percentage);

    }

    long sort(int[] array) {
        Instant start = Instant.now();
        Arrays.sort(array);
        Instant end = Instant.now();
        long elapsedTime = Duration.between(start, end).toMillis();
        System.out.printf("Time elapsed for sort: %s milliseconds%n", elapsedTime);

        return elapsedTime;
    }

    long parallelSort(int[] array) {
        Instant start = Instant.now();
        Arrays.parallelSort(array);
        Instant end = Instant.now();
        long elapsedTime = Duration.between(start, end).toMillis();
        System.out.printf("Time elapsed for parallel sort: %s milliseconds%n", elapsedTime);

        return elapsedTime;
    }
}
