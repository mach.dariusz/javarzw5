package com.sda.mach.dariusz.javarze5.wstepdojava.Day4.sorting;

import java.util.Arrays;
import java.util.Comparator;

public class App {

    public static void main(String[] args) {
        String[] names = {"Darek", "Robert", "Kasia", "Zuzia", "Paweł", "Mirek", "Gilbert"};
        System.out.println(Arrays.toString(names));

        // natural order
        Arrays.sort(names);

        System.out.println(Arrays.toString(names));

        // desc order
        Arrays.sort(names, new DESCOrder());
        System.out.println(Arrays.toString(names));

        // od najdluzszego do najkrotszego
        Arrays.sort(names, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o2.length() - o1.length();
            }
        });
        System.out.println(Arrays.toString(names));



    }
}

class DESCOrder implements Comparator<String> {

    @Override
    public int compare(String o1, String o2) {
        return o2.compareTo(o1);
    }
}
