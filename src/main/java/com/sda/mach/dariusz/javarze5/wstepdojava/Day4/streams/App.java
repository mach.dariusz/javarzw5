package com.sda.mach.dariusz.javarze5.wstepdojava.Day4.streams;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class App {

    public static void main(String[] args) {

        List<String> lista = Arrays.asList("Darek", "Mirek", "Kasia", "Robert", "Kasia", "Pawel");
        List<Integer> listaIntow = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 0);

        // for (int i = 0; i < lista.size(); i++) {
        //     if (lista.get(i).equals("Kasia")) System.out.println("Jest i Kasia");
        // }

        // filtrujemy wszyskie elementy, ktore sa rowne Kasia
        lista.stream().filter(new Predicate<String>() {
            @Override
            public boolean test(String imie) {
                return imie.equals("Kasia");
            }
        }).forEach(imie -> System.out.println(imie));

        // filtrujemy wszyskie elementy, ktore sa rowne Kasia
        lista.stream().filter(imie -> imie.equals("Kasia"))
                .forEach(imie -> System.out.println(imie));

        // filtrujemy wszyskie elementy, ktore sa rowne Kasia
        lista.stream().filter(wyraz -> wyraz.equals("Kasia")).forEach(System.out::println);


        // filtrujemy wszyskie elementy, ktore sa parzyste z listy: listaIntow
        listaIntow.stream().filter(new Predicate<Integer>() {
            @Override
            public boolean test(Integer liczba) {
                return liczba % 2 == 0;
            }
        }).forEach(liczba -> System.out.println(liczba));

        listaIntow.stream().filter(liczba -> liczba % 2 == 0)
                .forEach(liczba -> System.out.println(liczba));

        listaIntow.stream().filter(liczba -> liczba % 2 == 0)
                .forEach(System.out::println);



        lista.stream().forEach(System.out::println);

        String result = lista.stream()
                .filter(imie -> imie.equals("Gilbert"))
                .findFirst()
                .orElse("nie znalazlem takiego imienia");

        // System.out.println(optional.get());
        // System.out.println(optional.orElse("nie znalazlem takiego imienia"));

        System.out.println(result);

    }
}
