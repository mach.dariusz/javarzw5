package com.sda.mach.dariusz.javarze5.wstepdojava.Day4.zadanie;

public abstract class Activity implements DoingSomething {

    protected ActivityType activityType;

    public abstract void setActivityType();

    public ActivityType getActivityType() {
        return activityType;
    }

    public Activity() {
        this.setActivityType();
    }
}
