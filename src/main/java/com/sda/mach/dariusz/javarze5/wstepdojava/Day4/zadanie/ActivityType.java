package com.sda.mach.dariusz.javarze5.wstepdojava.Day4.zadanie;

public enum ActivityType {
    ZAJECIA {
        public void sayWhatYouDo() {
            System.out.println("Pracuje na zajeciach");
        }
    },
    NAUKA_W_DOMU {
        public void sayWhatYouDo() {
            System.out.println("Pracuje w domu");
        }
    },
    WOLNE {
        public void sayWhatYouDo() {
            System.out.println("Nie pracuje");
        }
    };

    public void sayWhatYouDo() {
        throw new AbstractMethodError();
    }

}
