package com.sda.mach.dariusz.javarze5.wstepdojava.Day4.zadanie;

import java.util.concurrent.TimeUnit;

public class NaZajeciach extends Activity {
    @Override
    public void setActivityType() {
        this.activityType = ActivityType.ZAJECIA;
    }

    @Override
    public void doThis() {
        System.out.println("Pracuje na zajeciach razem z prowadzacym ...");
        // ActivityType.ZAJECIA.sayWhatYouDo();
    }
}
