package com.sda.mach.dariusz.javarze5.wstepdojava.Day4.zadanie;

import java.util.ArrayList;
import java.util.List;

public class Student {

    private String firstName;
    private String lastName;
    private List<Activity> activities;

    public Student(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;

        this.activities = new ArrayList<>();
    }

    public void addActivity(Activity activity) {
        this.activities.add(activity);
    }

    public void executeActivities() {
        for (Activity activity: this.activities) {
            activity.doThis();
        }
    }
}
