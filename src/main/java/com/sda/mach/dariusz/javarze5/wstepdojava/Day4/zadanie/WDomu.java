package com.sda.mach.dariusz.javarze5.wstepdojava.Day4.zadanie;

public class WDomu extends Activity {
    @Override
    public void setActivityType() {
        this.activityType = ActivityType.NAUKA_W_DOMU;
    }

    @Override
    public void doThis() {
        System.out.println("Pracuje z domu ...");
        // ActivityType.NAUKA_W_DOMU.sayWhatYouDo();
    }
}
