package com.sda.mach.dariusz.javarze5.wstepdojava.Day4.zadanie;

public class Wolne extends Activity {
    @Override
    public void setActivityType() {
        this.activityType = ActivityType.WOLNE;
    }

    @Override
    public void doThis() {
        System.out.println("Dzis mam wolne ...");
        // ActivityType.WOLNE.sayWhatYouDo();
    }
}
