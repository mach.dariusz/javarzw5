package com.sda.mach.dariusz.javarze5.wstepdojava.Day1.rozgrzewka;

import org.junit.Assert;
import org.junit.Test;

public class AgeUtilsTest {

    @Test
    public void checkAge_passed18_shouldReturnTrue() throws Exception {
        Assert.assertTrue(AgeUtils.checkAge(18));
    }

    @Test
    public void checkAge_passed16_shouldReturnFalse() throws Exception {
        Assert.assertEquals(AgeUtils.checkAge(16), false);
    }

}