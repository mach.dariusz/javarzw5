package com.sda.mach.dariusz.javarze5.wstepdojava.Day3.segregator;

import com.sda.mach.dariusz.javarze5.wstepdojava.Day3.segregator.document.DokumentUS;
import com.sda.mach.dariusz.javarze5.wstepdojava.Day3.segregator.document.DokumentZUS;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Date;

import static org.junit.Assert.*;

public class SegregatorTest {

    private Segregator segregator;

    @Before
    public void executedBeforeEach() {
        this.segregator = new Segregator(Typ.MIESZANY);
        this.segregator.add(new Koszulka(new DokumentUS("US 1/1/2019", new Date())));
        this.segregator.add(new Koszulka(new DokumentUS("US 2/1/2019", new Date())));
        this.segregator.add(new Koszulka(new DokumentUS("US 3/1/2019", new Date())));
    }

    @Test
    public void add() {
        Assert.assertEquals(this.segregator.getAll().size(), 3);

        this.segregator.add(new Koszulka(new DokumentZUS("ZUS 1/2019", new Date())));
        Assert.assertEquals(this.segregator.getAll().size(), 4);
    }

    @Test
    public void get() {
        Assert.assertEquals(this.segregator.get(new DokumentUS("US 3/1/2019", new Date())).get(), new DokumentUS("US 3/1/2019", new Date()));
    }

    @Test
    public void remove() {

        this.segregator.remove(new DokumentUS("US 1/1/2019", new Date()));
        // System.out.println(this.segregator);
        Assert.assertEquals(this.segregator.getAll().size(), 2);
    }

}